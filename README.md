# Minecraft UI

Properties files for configuring user interface elements of the Minecraft plugins.

## Sounds

Defined in the `sounds` directory

Sounds are defined using the `sound:pitch:volume` format where:
* `sound` is a [Bukkit 1.8 sound name](https://gist.github.com/Andre601/1ab3b4fabd0010ae241156333491c379#file-mc_1-8-yml)
  Here is the list of the [minecraft 1.8 sound names](https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/mapping-and-modding-tutorials/2213619-1-8-all-playsound-sound-arguments)
* `pitch`
  * with one value is a singular pitch
  * with two values seperated by a `-` defines a sound that will be randomly pitched between the values every time it is played.
* `volume` is the volume of the sound

Pitch and volume must be between `0` and `2`.

If the string only has the sound defined, a pitch and volume of `1` will be used.
If the string only contains `sound:pitch` a volume of `1` will be used.


## Styles

Defined in the `styles` directory

Styles are defined using the following rules. Each rule can be seperated with a `;`.

### Simple Rules
* `bold`/`b` - bold text
* `italic`/`i` - italic text
* `underline`/`u` - underlined text
* `magic`/`m` - magic text
* `strike`/`s` - strikethrough text

### Complex Rules
Complex rules are a combination of an `id` and a value which are seperated by a `:`. These complex rules are as follows:

#### Colored Text
Colored text uses the `color` or `c` ID and takes a value of a Bukkit ChatColor name.

#### Click Event
You can use click events to define what should happen when text is clicked. Click events use the `click` & `clickevent` IDs  and are defined in the `type-value` format where `type` is a [click event type](https://github.com/SpigotMC/BungeeCord/blob/master/chat/src/main/java/net/md_5/bungee/api/chat/ClickEvent.java#L24) and value is value of the type.
